import { combineReducers } from 'redux'
import { REQUEST_USERS, RECEIVE_USERS } from './actions'

function users(state = { loading: false, list: [] }, action) {
  switch (action.type) {
    case REQUEST_USERS:
      return Object.assign({}, state, { loading: true })
    case RECEIVE_USERS:
      return Object.assign({}, state, { loading: false, list: action.users })
    default:
      return state
  }
}

export default combineReducers({
  users
})
