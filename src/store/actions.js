import ls from '../data'

export const REQUEST_USERS = 'REQUEST_USERS'
export const RECEIVE_USERS = 'RECEIVE_USERS'
// export const GET_USER = 'GET_USER'
export const REQUEST_ADD_USER = 'REQUEST_ADD_USER'

function requestUsers() {
  return {
    type: REQUEST_USERS
  }
}

function receiveUsers(users) {
  return {
    type: RECEIVE_USERS,
    users
  }
}

export function getUsers() {
  return dispatch => {
    dispatch(requestUsers)

    return ls
      .getUsers()
      .then(
        users => dispatch(receiveUsers(users)),
        e => console.error('Error when getting users:', e)
      )
  }
}

export function addUser(user) {
  return dispatch => {
    return ls
      .addUser(user)
      .then(
        () => dispatch(getUsers()),
        e => console.error('Error when adding user:', e)
      )
  }
}

export function editUser(user) {
  return dispatch => {
    return ls
      .editUser(user)
      .then(
        () => dispatch(getUsers()),
        e => console.error('Error when editing user:', e)
      )
  }
}

export function deleteUser(userId) {
  return dispatch => {
    return ls
      .deleteUser(userId)
      .then(
        () => dispatch(getUsers()),
        e => console.error('Error when deleting user:', e)
      )
  }
}
