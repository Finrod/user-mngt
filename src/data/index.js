// Promise base local storage management to act like HTTP requests

import { filter, orderBy } from 'lodash-es'
import json from './users.json'

// Add initial data to local storage
export function initData() {
  const users = localStorage.getItem('users')
  if (!users) localStorage.setItem('users', JSON.stringify(json))
}

// Get users
export function getUsers() {
  const users = JSON.parse(localStorage.getItem('users'))
  return Promise.resolve(orderBy(users, ['firstname', 'lastname']))
}

// Add user
export function addUser(user) {
  function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c === 'x' ? r : (r & 0x3) | 0x8
      return v.toString(16)
    })
  }

  const users = JSON.parse(localStorage.getItem('users'))
  users.push({
    ...user,
    guid: user.guid || uuidv4()
  })
  localStorage.setItem('users', JSON.stringify(users))
  return Promise.resolve()
}

// Edit user
export function editUser(user) {
  deleteUser(user.guid)
  addUser(user)
  return Promise.resolve()
}

// Delete user
export function deleteUser(userId) {
  const users = JSON.parse(localStorage.getItem('users'))
  const filteredUsers = filter(users, u => u.guid !== userId)
  localStorage.setItem('users', JSON.stringify(filteredUsers))
  return Promise.resolve()
}

export default {
  initData,
  getUsers,
  addUser,
  editUser,
  deleteUser
}
