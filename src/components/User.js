import React from 'react'
import PropTypes from 'prop-types'

import { MdDelete, MdEdit } from 'react-icons/md'

class User extends React.Component {
  render() {
    const { user, onEditUser, onDeleteUser } = this.props

    return (
      <a
        key={`#${user.guid}`}
        href={`#${user.guid}`}
        className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
      >
        <span>
          {user.firstname} {user.lastname}
        </span>

        <span>{user.email}</span>

        <span>
          {!!this.props.onEditUser && (
            <MdEdit className='mr-2' size={18} onClick={() => onEditUser()} />
          )}
          {!!this.props.onDeleteUser && (
            <MdDelete size={18} onClick={() => onDeleteUser()} />
          )}
        </span>
      </a>
    )
  }
}

User.propTypes = {
  user: PropTypes.object.isRequired,
  onEditUser: PropTypes.func,
  onDeleteUser: PropTypes.func
}

export default User
