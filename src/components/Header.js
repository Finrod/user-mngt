import React from 'react'

class Header extends React.Component {
  render() {
    return <h1 className='mt-4 text-center'>User management</h1>
  }
}

export default Header
