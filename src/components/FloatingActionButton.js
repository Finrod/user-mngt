import React from 'react'
import PropTypes from 'prop-types'

import { MdAdd, MdEdit } from 'react-icons/md'

import './FloatingActionButton.css'

class FloatinActionButton extends React.Component {
  render() {
    return (
      <div className='fab' onClick={() => this.props.onClick()}>
        {this.props.type === 'add' && <MdAdd size={24} />}
        {this.props.type === 'edit' && <MdEdit size={24} />}
      </div>
    )
  }
}

FloatinActionButton.propTypes = {
  type: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

export default FloatinActionButton
