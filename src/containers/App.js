import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getUsers, deleteUser } from '../store/actions'

import Header from '../components/Header'
import User from '../components/User'
import UserModal from '../containers/UserModal'
import FloatingActionButton from '../components/FloatingActionButton'

const emptyUser = {
  guid: '',
  firstname: '',
  lastname: '',
  email: ''
}

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      userModalShown: false,
      userToEdit: emptyUser
    }
  }

  componentDidMount() {
    this.props.dispatch(getUsers())
  }

  openAddModal = () => {
    this.setState({ userModalShown: true })
  }

  handleAddModalClose = () => {
    this.setState({ userModalShown: false, userToEdit: emptyUser })
  }

  editUser(user) {
    this.setState({ userModalShown: true, userToEdit: user })
  }

  deleteUser(userId) {
    this.props.dispatch(deleteUser(userId))
  }

  render() {
    return (
      <div>
        <Header />

        <main className='w-75 mx-auto'>
          <div className='list-group'>
            {this.props.users.map(u => (
              <User
                key={u.guid}
                user={u}
                onEditUser={() => this.editUser(u)}
                onDeleteUser={() => this.deleteUser(u.guid)}
              />
            ))}
          </div>
        </main>

        <FloatingActionButton type='add' onClick={this.openAddModal} />

        <UserModal
          show={this.state.userModalShown}
          userToEdit={this.state.userToEdit}
          onHide={this.handleAddModalClose}
        />
      </div>
    )
  }
}

App.propTypes = {
  users: PropTypes.array.isRequired
}

const mapStateToProps = state => {
  return {
    users: state.users.list || []
  }
}

export default connect(mapStateToProps)(App)
