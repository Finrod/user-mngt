import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addUser, editUser } from '../store/actions'
import * as Yup from 'yup'

import { Formik, Field, Form } from 'formik'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

const UserSchema = Yup.object().shape({
  firstname: Yup.string()
    .min(2, 'The first name is too short')
    .max(50, 'The first name is too long')
    .required('The first name is required'),
  lastname: Yup.string()
    .min(2, 'The last name is too short')
    .max(50, 'The last name is too long')
    .required('The last name is required'),
  email: Yup.string()
    .email('The email is not valid')
    .required('The email is required')
})

class UserModal extends React.Component {
  save = values => {
    let savePromise = null
    if (this.props.userToEdit.guid) {
      savePromise = this.props.dispatch(
        editUser({
          ...this.props.userToEdit,
          firstname: values.firstname,
          lastname: values.lastname,
          email: values.email
        })
      )
    } else {
      savePromise = this.props.dispatch(
        addUser({
          firstname: values.firstname,
          lastname: values.lastname,
          email: values.email
        })
      )
    }

    return savePromise
  }

  render() {
    const { onHide } = this.props
    const { firstname, lastname, email } = this.props.userToEdit

    return (
      <Modal show={this.props.show} onShow={this.onShow} onHide={onHide}>
        <Formik
          initialValues={{ firstname, lastname, email }}
          validationSchema={UserSchema}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(true)
            this.save(values).then(() => {
              setSubmitting(false)
              onHide()
            })
          }}
          render={({ errors, touched, isSubmitting }) => (
            <Form>
              <Modal.Header>
                <Modal.Title>Add user</Modal.Title>
              </Modal.Header>

              <Modal.Body>
                <div className='form-row'>
                  <div className='form-group col'>
                    <label htmlFor='firstname'>First name *</label>
                    <Field
                      type='text'
                      id='firstname'
                      name='firstname'
                      className={
                        'form-control' +
                        (errors.firstname && touched.firstname
                          ? ' is-invalid'
                          : '')
                      }
                    />
                    <div className='invalid-feedback'>{errors.firstname}</div>
                  </div>

                  <div className='form-group col'>
                    <label htmlFor='lastname'>Last name *</label>
                    <Field
                      type='text'
                      id='lastname'
                      name='lastname'
                      className={
                        'form-control' +
                        (errors.lastname && touched.lastname
                          ? ' is-invalid'
                          : '')
                      }
                    />
                    <div className='invalid-feedback'>{errors.lastname}</div>
                  </div>
                </div>

                <div className='form-group'>
                  <label htmlFor='email'>Email *</label>
                  <Field
                    type='email'
                    id='email'
                    name='email'
                    className={
                      'form-control' +
                      (errors.email && touched.email ? ' is-invalid' : '')
                    }
                  />
                  <small className='form-text text-muted'>
                    We'll never share your email with anyone else
                  </small>
                  <div className='invalid-feedback'>{errors.email}</div>
                </div>
              </Modal.Body>

              <Modal.Footer>
                <Button variant='link' onClick={onHide}>
                  Close
                </Button>

                <Button variant='link' type='submit' disabled={isSubmitting}>
                  {isSubmitting ? 'Saving...' : 'Save'}
                </Button>
              </Modal.Footer>
            </Form>
          )}
        />
      </Modal>
    )
  }
}

UserModal.propTypes = {
  show: PropTypes.bool.isRequired,
  userToEdit: PropTypes.object,
  onHide: PropTypes.func.isRequired
}

export default connect()(UserModal)
