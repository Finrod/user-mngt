import React from 'react'
import ReactDOM from 'react-dom'
import { initData } from './data'
import { Provider } from 'react-redux'
import store from './store'

import App from './containers/App'

import './bootstrap.min.css'

// Initialize localstorage with data if empty
initData()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
